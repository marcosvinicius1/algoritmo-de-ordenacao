import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		   Scanner teclado = new Scanner(System.in);
			int[] vetordesordenado = gerarvetor(10);
			int[] vetorordenado = new int [10];
			
			
			System.out.println("Algoritmos de Ordena��o\n");
			System.out.println("Escolha qual metodo deseja usar:");
			System.out.println("1 -> BubbleSORT");
			System.out.println("2 -> SelectionSORT");
			System.out.println("3 -> InsertionSORT");
			System.out.println("4 -> MergeSORT");
			System.out.println("5 -> QuickSORT");
		
			
			int opcao = teclado.nextInt();
			teclado.close();
		
			
			switch (opcao) {
			case 1:
				System.out.println("<====Vetor desordenado====>");
				System.out.println(Arrays.toString(vetordesordenado));
				System.out.println("\nVetor ordenado pelo m�todo: BubbleSORT");
				vetorordenado = bubbleSort(vetordesordenado);
				for(int i : vetorordenado) {
					System.out.println(i+ " ");
					}
				break;
				
			case 2:
				System.out.println("<====Vetor desordenado====>");
				System.out.println(Arrays.toString(vetordesordenado));
				System.out.println("\nVetor ordenado pelo m�todo: SelectionSORT");
				vetorordenado = selectionSORT(vetordesordenado);
				for(int i : vetorordenado) {
					System.out.println(i+ " ");
					}
				break;
				
			case 3:
				System.out.println("<====Vetor desordenado====>");
				System.out.println(Arrays.toString(vetordesordenado));
				System.out.println("\nVetor ordenado pelo m�todo: InsetionSORT");
				vetorordenado = insertionSort(vetordesordenado);
				for(int i : vetorordenado) {
					System.out.println(i+ " ");
					}
				break;
				
			case 4:
				System.out.println("<====Vetor desordenado====>");
				System.out.println(Arrays.toString(vetordesordenado));
				System.out.println("\nVetor ordenado pelo m�todo: mergeSORT");
				vetorordenado = mergeSort(vetordesordenado, new int[vetordesordenado.length], 0, vetordesordenado.length-1);
				for(int i : vetorordenado) {
					System.out.println(i+ " ");
				}
				break;
				
			case 5:
				System.out.println("<====Vetor desordenado====>");
				System.out.println(Arrays.toString(vetordesordenado));
				System.out.println("\nVetor ordenado pelo m�todo: quickSORT");
				vetorordenado = quickSort(vetordesordenado,0,vetordesordenado.length-1);
				for(int i : vetorordenado) {
					System.out.println(i+ " ");
					}
				break;
				
			default :
				System.out.println("\nOp��o n�o existente");
				break;	
			}
		}
	
//bubble
		private static int[] bubbleSort(int[] vetor) {
			int n = vetor.length;
			for (int i = 0; i<n ; i++) {
				for(int j = 0 ; j < n-1 ; j++) {
					if (vetor[j]> vetor[j+1]) {
						int aux = vetor[j];
						vetor[j] = vetor [j+1];
						vetor [j+1]= aux;
					} 
				}
			}
			return vetor;
		}
		
		
//selection
		private static int[] selectionSORT(int[] vetor) {
		
			for (int i = 0; i < vetor.length; i++) {
			int menor = i;
			for (int j = i + 1; j < vetor.length; j++) {
				if (vetor[j] < vetor[menor]) {
					menor = j;
					}
				}
			
			int aux = vetor[menor];
			vetor[menor] = vetor[i];
			vetor[i] = aux;
			}
			
			return vetor;
		}
		
//insertion
		private static int[] insertionSort(int[] vetor) {
			int aux, n;
			for (int i =1; i<vetor.length; i++) {
				aux = vetor[i];
				n = i-1;
				
			while ((n>=0) && (vetor[n]> aux)) {
				vetor[n+1] = vetor[n];
				n--;
				}
			vetor[n+1] = aux;
			}
			return vetor;
		}
		
//merge
		private static int[] mergeSort(int[] vetorD, int[] w, int inicio, int fim) {
			if (inicio<fim) {
				int meio = (inicio+fim)/2;
				mergeSort(vetorD, w, inicio, meio);
				mergeSort(vetorD, w, meio+1, fim);
				intercalar(vetorD, w, inicio, meio, fim);
			}
			
			return vetorD;
		}
		private static void intercalar(int[] vetorD, int[] w, int inicio, int meio, int fim) {
			//COPIANDO O VETORDESORDENADO PARA O VETOR K
			for (int k = inicio; k<= fim; k++)
				w[k] = vetorD[k];
			
			int i = inicio;
			int j = meio + 1;
			
			for (int k = inicio; k<= fim; k++) {
				if(i > meio) vetorD[k] = w[j++];
				else if (j > fim) vetorD[k] = w [i];
				else if (w[i] < w[j]) vetorD[k] = w[i++];
				else vetorD[k] = w[j++];
			}
			
		}
		
//quick
		private static int[] quickSort(int[] vetord, int esq, int dir) {
			if (esq < dir) {
				int j = separar(vetord, esq, dir);
				quickSort(vetord, esq, j-1);
				quickSort(vetord, j+1, dir);
			}
			return vetord;
		}

		private static int separar(int[] vetord, int esq, int dir) {
			int i = esq+1;
			int j = dir;
			int pivo = vetord[esq];
			while(i <= j) {
				if(vetord[i] <= pivo) i++;
				else if (vetord[j] > pivo) j--;
				else if (i <= j) {
					trocar(vetord, i, j);
					i++;
					j--;
				}
			}
			trocar(vetord, esq, j);
			return j;
		}

		private static void trocar(int[] vetord, int i, int j) {
			int aux = vetord[i];
			vetord[i] = vetord[j];
			vetord[j] = aux;
			
		}
	
//criar numeros random
		
private static int[] gerarvetor(int tamanho) {
	int[] vetorNaoOrdenado = new int[tamanho];
	Random gerar = new Random();
	for (int i = 0; i <vetorNaoOrdenado.length; i++) {
		vetorNaoOrdenado[i] = gerar.nextInt(20);
	}
	
	return vetorNaoOrdenado;
}

	
}



